import ReactDOM from 'react-dom';
import React, { useEffect } from 'react';
import { Provider, connect } from 'react-redux';
import './scss/index.scss';
import store from './app/store';
import App from './app/App';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import firebase from 'firebase/app';
const rootElement = document.getElementById('app');

const firebaseConfig = {
  apiKey: 'AIzaSyBmzec4nscP0_gKLtNloyh_Z7mtGQwQgPg',
  authDomain: 'education-f6bc7.firebaseapp.com',
  projectId: 'education-f6bc7',
  storageBucket: 'education-f6bc7.appspot.com',
  messagingSenderId: '603689630660',
  appId: '1:603689630660:web:765f3c9ca808d119200291',
};

firebase.initializeApp(firebaseConfig);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  rootElement,
);
