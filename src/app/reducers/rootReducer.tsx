import { SET_INITIAL_LIMIT, SUBMIT_TO_LOGIN } from '../actions/actions';
import { LOGIN_SUCCESS } from '../actions/actions';
import { SET_CHAT_LIST } from '../actions/actions';
import { GET_SEARCH } from '../actions/actions';
import { SET_HAS_MORE_CHATS } from '../actions/actions';
import { SET_CHATS_FOR_SEARCHING } from '../actions/actions';
import { SET_DIALOG_CONTENT } from '../actions/actions';
import { SET_FULL_CHAT_LIST } from '../actions/actions';
import { CLEAR_CHATS_FOR_SEARCHING } from '../actions/actions';
const rootReducer = (
  state: {
    password: '';
    email: '';
    token: string;
    limit: number;
    moreChats: boolean;
    chatList: any;
    dialogContent: any;
    chatsForSearching: any;
    isSearch: boolean;
    fullChats: any;
  },
  action: { type: string; email: string; password: string; limit: number; payload?: any },
) => {
  switch (action.type) {
    case SUBMIT_TO_LOGIN: {
      return { ...state, email: action.email, password: action.password };
    }
    case LOGIN_SUCCESS: {
      return { ...state, token: action.payload.token };
    }

    case SET_HAS_MORE_CHATS: {
      return { ...state, moreChats: action.payload.moreChats };
    }
    case SET_CHAT_LIST: {
      return { ...state, chatList: action.payload, dialogContent: [] };
    }
    case SET_DIALOG_CONTENT: {
      return { ...state, dialogContent: action.payload };
    }
    case SET_CHATS_FOR_SEARCHING: {
      return { ...state, chatsForSearching: action.payload };
    }
    case GET_SEARCH: {
      return { ...state, isSearch: action.payload };
    }
    case CLEAR_CHATS_FOR_SEARCHING: {
      return { ...state, chatsForSearching: [] };
    }
    case SET_FULL_CHAT_LIST: {
      return { ...state, fullChats: action.payload };
    }
    default:
      return state;
  }
};

export default rootReducer;
