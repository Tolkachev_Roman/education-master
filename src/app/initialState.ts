export const initialState: {
  password: string;
  email: string;
  token: string;
  limit: number;
  moreChats: boolean;
  chatList: [];
  messages: [];
  chatsForSearching: [];
  isSearch: boolean;
} = {
  password: '',
  email: '',
  token: '',
  limit: 8,
  moreChats: true,
  chatList: [],
  messages: [],
  chatsForSearching: [],
  isSearch: false,
};
