import { call, put, takeLatest } from 'redux-saga/effects';
import firebase from 'firebase';
import { SUBMIT_TO_LOGIN } from '../actions/actions';
import { EXIT } from '../actions/actions';
import { GET_CHATS_FOR_SEARCHING } from '../actions/actions';
import { SET_SEARCH } from '../actions/actions';
import { GET_ALL_CHATS } from '../actions/actions';
import { SET_FULL_CHAT_LIST } from '../actions/actions';
import { GET_SEARCH } from '../actions/actions';
import { SET_CHATS_FOR_SEARCHING } from '../actions/actions';
import { LOGIN_SUCCESS } from '../actions/actions';

function* workerSaga(action: any) {
  yield firebase
    .auth()
    .signInWithEmailAndPassword(action.email, action.password)
    .then((userCredential) => {
      alert('Аутентификация прошла успешно');
      if (!localStorage.token) {
        localStorage.setItem('token', userCredential.user.refreshToken);
      }
    })
    .catch((error) => {
      alert(`Аутентификация провалена: ${error.message}`);
    });

  const token = localStorage.getItem('token');
  yield put({ type: LOGIN_SUCCESS, payload: { token: token } });
}

function* exitWorker() {
  localStorage.removeItem('token');
  const token = '';
  yield put({ type: LOGIN_SUCCESS, payload: { token: token } });
}

export interface ResponseGenerator {
  limit: number;
  length: ResponseGenerator;
  data?: any;
}

async function chatsService() {
  const result = await firebase
    .database()
    .ref('/dialogs/')
    .get()
    .then(async (snapshot) => {
      return snapshot.val();
    })
    .catch((error) => console.log('ERROR', error));
  return result;
}

function* searchWorker(action: any) {
  try {
    const data: any = yield call(chatsService);
    const items: any = [];
    data.forEach((item: any) => {
      const elementForSearch = action.element;
      const regular = new RegExp(elementForSearch, 'i');
      if (item.clientName.match(regular) !== null) {
        return items.push(item);
      } else {
        item.messages.forEach((elem: any) => {
          if (typeof elem.content != 'object') {
            if (elem.content.match(regular) !== null) return items.push(item);
          }
        });
      }
    });

    const filteredItems = items.filter(function (item: any, pos: any) {
      return items.indexOf(item) == pos;
    });
    yield put({ type: SET_CHATS_FOR_SEARCHING, payload: filteredItems });
  } catch (error) {
    console.log(error);
  }
}

function* getSearchWorker(action: any) {
  yield put({ type: GET_SEARCH, payload: action.payload });
}

function* allChatsWorker() {
  const data: any = yield call(chatsService);
  yield put({ type: SET_FULL_CHAT_LIST, payload: data });
}

function* mySaga() {
  yield takeLatest(SUBMIT_TO_LOGIN, workerSaga);
  yield takeLatest(EXIT, exitWorker);
  yield takeLatest(GET_CHATS_FOR_SEARCHING, searchWorker);
  yield takeLatest(SET_SEARCH, getSearchWorker);
  yield takeLatest(GET_ALL_CHATS, allChatsWorker);
}

export default mySaga;
