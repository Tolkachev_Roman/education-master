import React from 'react';
import SignupForm_w from '../components/signUpForm';
import MainPage from '../components/MainPage';
import { Switch, Route } from 'react-router';
const App = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={SignupForm_w}></Route>
        <Route exact path="/MainPage" component={MainPage} />
      </Switch>
    </div>
  );
};

export default App;
