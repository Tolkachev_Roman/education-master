import { CLEAR_CHATS_FOR_SEARCHING } from '../actions/actions';

export const clearChatsForSearching = () => {
  return {
    type: CLEAR_CHATS_FOR_SEARCHING,
  };
};
