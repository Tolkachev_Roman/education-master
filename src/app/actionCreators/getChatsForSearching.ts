import { GET_CHATS_FOR_SEARCHING } from '../actions/actions';

export const getChatsForSearching = (element: any) => {
  return {
    type: GET_CHATS_FOR_SEARCHING,
    element,
  };
};
