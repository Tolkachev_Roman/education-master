import { SUBMIT_TO_LOGIN } from '../actions/actions';
export const submitToLogin = (password: string, email: string) => {
  return {
    type: SUBMIT_TO_LOGIN,
    password: password,
    email: email,
  };
};
