import { SET_CHAT_LIST } from '../actions/actions';
export const setChatList = (payload: any[]) => {
  return {
    type: SET_CHAT_LIST,
    payload,
  };
};
