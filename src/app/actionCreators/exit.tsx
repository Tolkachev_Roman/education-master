import React from 'react';
import { EXIT } from '../actions/actions';
export const exit = () => {
  return {
    type: EXIT,
  };
};
