import { SET_SEARCH } from '../actions/actions';
export const setSearch = (payload: any) => {
  return {
    type: SET_SEARCH,
    payload,
  };
};
