import { SET_DIALOG_CONTENT } from '../actions/actions';
export const setDialogContent = (payload: { dialogContent: any }) => {
  return {
    type: SET_DIALOG_CONTENT,
    payload,
  };
};
