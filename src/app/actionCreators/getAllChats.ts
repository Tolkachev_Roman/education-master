import { GET_ALL_CHATS } from '../actions/actions';

export const getAllChats = () => {
  return {
    type: GET_ALL_CHATS,
  };
};
