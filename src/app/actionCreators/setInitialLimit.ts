import { SET_INITIAL_LIMIT } from '../actions/actions';
export const setInitialLimit = () => {
  return {
    type: SET_INITIAL_LIMIT,
  };
};
