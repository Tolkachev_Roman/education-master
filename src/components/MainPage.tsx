import React, { useState } from 'react';
import Layout from './Layout';
import DialogContentArea_w from './DialogContentArea';
import SavedList_w from './SavedList';
import FinishedList_w from './FinishedList';
import QueueList_w from './QueueList';
import List_w from './List';

const MainPage = (props: any) => {
  const [listType, setlistType] = useState('main');

  return (
    <Layout>
      <div>
        <div className="category">
          <button
            className="btn_main"
            onClick={(ev) => {
              ev.preventDefault();
              setlistType('main');
            }}
          >
            All chats
          </button>
          <button
            className="btn_saved"
            onClick={(ev) => {
              ev.preventDefault();
              setlistType('saved');
            }}
          >
            Saved chats
          </button>
          <button
            className="btn_finished"
            onClick={(ev) => {
              ev.preventDefault();
              setlistType('finished');
            }}
          >
            Finished chats
          </button>
          <button
            className="btn_queue"
            onClick={(ev) => {
              ev.preventDefault();
              setlistType('queue');
            }}
          >
            Queue of chats
          </button>
        </div>
        {listType === 'main' && (
          <div>
            {' '}
            <List_w />{' '}
          </div>
        )}
        {listType === 'saved' && (
          <div>
            {' '}
            <SavedList_w />{' '}
          </div>
        )}
        {listType === 'finished' && (
          <div>
            {' '}
            <FinishedList_w />{' '}
          </div>
        )}
        {listType === 'queue' && (
          <div>
            {' '}
            <QueueList_w />{' '}
          </div>
        )}
      </div>
      <DialogContentArea_w />
    </Layout>
  );
};

export default MainPage;
