import React from 'react';

const DialogMessage = (props: any) => {
  if (typeof props.text !== 'string') {
    return <img src={props.text.url} alt="Картинка" />;
  } else
    return (
      <div className="dialog-message">
        <p className="dialog-message__text">{props.text}</p>
      </div>
    );
};

export default DialogMessage;
