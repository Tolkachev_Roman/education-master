import React from 'react';
import { useFormik } from 'formik';
import { connect } from 'react-redux';
import '../scss/index.scss';
import { submitToLogin } from '../app/actionCreators/submitToLogin';
import { AnyAction, bindActionCreators, Dispatch } from 'redux';
import { Redirect } from 'react-router';

const validate = (values: { password: string; email: string }) => {
  const errors: { password: string; email: string } = { password: '', email: '' };

  if (!values.password) {
    errors.password = 'Required';
    return errors;
  } else if (values.password.length > 20) {
    errors.password = 'Must be 20 characters or less';
    return errors;
  }

  if (!values.email) {
    errors.email = 'Required';
    return errors;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
    return errors;
  }
};

const SignupForm = (props: any) => {
  const formik = useFormik({
    initialValues: {
      password: '',
      email: '',
    },
    validate,
    onSubmit: (values) => {
      props.submitToLogin(values.password, values.email);
    },
  });

  if (localStorage.token) return <Redirect to="/MainPage" />;

  return (
    <div className="wrap">
      <div className="form-container">
        <h2 className="heading">LOGIN</h2>

        <form onSubmit={formik.handleSubmit} className="sign-form">
          <div className="inputs">
            <div className="inputs__item">
              <label htmlFor="email" className="sign-form__label">
                Email Address
              </label>
              <input
                className="sign-form__inp"
                id="email"
                name="email"
                type="email"
                onChange={formik.handleChange}
                value={formik.values.email}
              />
              <i className="fas fa-envelope mail-icon"></i>
            </div>
            {formik.touched.email && formik.errors.email ? <div className="error">{formik.errors.email}</div> : null}

            <div className="inputs__item">
              <label htmlFor="password" className="sign-form__label">
                Password
              </label>
              <input
                className="sign-form__inp"
                id="password"
                name="password"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.password}
              />
              <i className="fas fa-lock fa-lg pass-icon"></i>
              {formik.touched.password && formik.errors.password ? (
                <div className="error">{formik.errors.password}</div>
              ) : null}
            </div>
          </div>
          <button type="submit" className="sign-form__btn">
            Log in
          </button>
        </form>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
  return bindActionCreators(
    {
      submitToLogin: submitToLogin,
    },
    dispatch,
  );
};

const SignupForm_w = connect(null, mapDispatchToProps)(SignupForm);

export default SignupForm_w;
