import ExitBtn from './ExitBtn';
import React from 'react';

const Header = () => {
  return (
    <header className="header">
      <h2 className="header__title">Main page</h2>
      <ExitBtn />
    </header>
  );
};

export default Header;
