import React, { useState } from 'react';
import { setChatList } from '../app/actionCreators/setChatList';
import { bindActionCreators, Dispatch } from 'redux';
import { setDialogContent } from '../app/actionCreators/setDialogContent';
import { connect } from 'react-redux';
import { getChatsForSearching } from '../app/actionCreators/getChatsForSearching';
import { setSearch } from '../app/actionCreators/setSearch';
import { clearChatsForSearching } from '../app/actionCreators/clearChatsForSearching';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const _ = require('lodash');

const SearchDialogForm = (props: any) => {
  const [dialogs, setDialogs] = useState({ value: 0 });
  const [elementForSearch, setElementForSearch] = useState('');
  const [searchResult, setSearchResult] = useState(undefined);

  const getDialogs = (value: any) => {
    props.clearChatsForSearching();

    props.setSearch(true);
    props.getAllChats(value);
  };

  const search = (elem: any) => {
    getDialogs(elem);
  };

  const debounceSearch = (value: any) => {
    try {
      _.debounce(search(value), 300);
    } catch (error) {
      return;
    }
  };

  return (
    <form action="#" className="search-dialogs">
      <input
        type="text"
        placeholder="Поиск"
        className="search-dialogs__inp"
        onKeyUp={(ev: any) => debounceSearch(ev.target.value)}
      />
    </form>
  );
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators(
    {
      setChatList: setChatList,
      setDialogContent: setDialogContent,
      getAllChats: getChatsForSearching,
      setSearch: setSearch,
      clearChatsForSearching: clearChatsForSearching,
    },
    dispatch,
  );
};

const mapStateToProps = (state: any) => {
  return {
    state: state,
  };
};

const SearchDialogForm_w = connect(mapStateToProps, mapDispatchToProps)(SearchDialogForm);

export default SearchDialogForm_w;
