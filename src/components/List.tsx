import InfiniteScroll from 'react-infinite-scroller';
import Dialog from './Dialog';
import React, { useState } from 'react';
import firebase from 'firebase';
import { bindActionCreators, Dispatch } from 'redux';
import { setChatList } from '../app/actionCreators/setChatList';
import { connect } from 'react-redux';
import { setDialogContent } from '../app/actionCreators/setDialogContent';
import SearchDialogForm_w from './SearchDialogForm';
import { setSearch } from '../app/actionCreators/setSearch';
const List = (props: any) => {
  const [moreChats, setMoreChats] = useState(true);
  const [dialogs, setDialogs] = useState([]);

  if (props.chatsForSearching.length > 0 && moreChats != false) {
    setMoreChats(false);
    setDialogs(props.chatsForSearching);
  }
  if (props.chatsForSearching.length === 0 && moreChats != true) {
    setMoreChats(true);
  }

  const showContent = (key: any) => {
    dialogs.map((item: any, i: any) => {
      if (key === i) {
        item = {
          key: key,
          content: item,
        };

        props.setDialogContent(item);
      }
    });
  };

  const setStatus = (key: any, checkbox: any) => {
    if (checkbox.getAttribute('id') === 'dialog-preview__checkbox_save') {
      checkbox.classList.toggle('checked');
      if (checkbox.classList.contains('checked'))
        try {
          firebase
            .database()
            .ref('/dialogs/' + key)
            .update({ isSaved: true });
        } catch (error) {
          console.log(error);
        }
      else {
        try {
          firebase
            .database()
            .ref('/dialogs/' + key)
            .update({ isSaved: false });
        } catch (error) {
          console.log(error);
        }
      }
    }

    if (checkbox.getAttribute('id') === 'dialog-preview__checkbox_finished') {
      checkbox.classList.toggle('checked');
      if (checkbox.classList.contains('checked'))
        try {
          firebase
            .database()
            .ref('/dialogs/' + key)
            .update({ status: 'finished' });
        } catch (error) {
          console.log(error);
        }
      else {
        try {
          firebase
            .database()
            .ref('/dialogs/' + key)
            .update({ status: 'default' });
        } catch (error) {
          console.log(error);
        }
      }
    }
    firebase
      .database()
      .ref('/dialogs/' + key)
      .get()
      .then((snapshot) => {
        console.log('NEW DATA', snapshot.val());
      });
  };

  const getAmountOfDialogs = () => {
    const result = firebase
      .database()
      .ref('/dialogs/')
      .get()
      .then((snapshot) => {
        const amountOfChats = snapshot.numChildren();
        return amountOfChats;
      });
    return result;
  };

  const getDialogs = ({ limit }: any) => {
    firebase
      .database()
      .ref('/dialogs/')
      .limitToFirst(limit)
      .get()
      .then(async (snapshot) => {
        const data = snapshot.val();
        console.log('DATA', data);
        setDialogs(data);

        const amountOfDialogs = await getAmountOfDialogs();

        dialogs.length === amountOfDialogs ? setMoreChats(false) : setMoreChats(true);
      })
      .catch((error) => console.log('ERROR', error));
  };

  return (
    <div className="dialogs-container">
      <SearchDialogForm_w />
      <InfiniteScroll
        pageStart={0}
        threshold={0}
        loadMore={() =>
          getDialogs({
            limit: dialogs.length + 8,
          })
        }
        loader={<p key={0}>Loader...</p>}
        hasMore={moreChats}
        useWindow={false}
      >
        {dialogs.map((dialog: any, key: any) => {
          return (
            <Dialog
              key={key}
              name={dialog.clientName}
              text={dialog.messages[dialog.messages.length - 1].content}
              showContent={showContent.bind(this, key)}
              setStatus={setStatus.bind(this, key)}
              status={dialog.status}
              isSaved={dialog.isSaved}
            />
          );
        })}
      </InfiniteScroll>
    </div>
  );
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators(
    {
      setChatList: setChatList,
      setDialogContent: setDialogContent,
      setSearch: setSearch,
    },
    dispatch,
  );
};

const mapStateToProps = (state: any) => {
  return {
    chatsForSearching: state.chatsForSearching,
    chatList: state.chatList,
    isSearch: state.isSearch,
  };
};

const List_w = connect(mapStateToProps, mapDispatchToProps)(List);

export default List_w;
