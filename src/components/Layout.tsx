import Header from './Header';
import React from 'react';

const Layout = ({ children }: any) => {
  return (
    <div className="wrap">
      <Header />

      <div className="container">{children}</div>
    </div>
  );
};

export default Layout;
