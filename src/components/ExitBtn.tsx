import React, { useEffect } from 'react';
import { AnyAction, bindActionCreators, Dispatch } from 'redux';
import { Redirect, Switch, Route, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { exit } from '../app/actionCreators/exit';

const ExitBtn = (props: any) => {
  if (!localStorage.token) {
    return <Redirect to="/" />;
  }

  return <button onClick={() => props.exit()}>EXIT</button>;
};

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
  return bindActionCreators(
    {
      exit: exit,
    },
    dispatch,
  );
};

const mapStateToProps = (state: any) => {
  return {
    state: state,
  };
};

const ExitBtn_w = connect(mapStateToProps, mapDispatchToProps)(ExitBtn);

export default ExitBtn_w;
