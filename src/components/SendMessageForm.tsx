import React, { useState } from 'react';
import firebase from 'firebase';

const SendMessageForm = (props: any) => {
  const [value, setValue] = useState();
  const send = (message: string) => {
    console.log('send', props.clientKey);
    try {
      firebase
        .database()
        .ref('/dialogs/' + props.clientKey + '/messages')
        .push({
          content: message,
          theme: 'Налоги',
          timestamp: 1604408031,
          writtenBy: 'client',
        });
    } catch (error) {
      console.log(error);
    }
    firebase
      .database()
      .ref('/dialogs/' + props.clientKey)
      .get()
      .then((snapshot) => {
        console.log('NEW DATA', snapshot.val());
      });
  };

  return (
    <div className="send-message">
      <form className="send-message__form">
        <input
          placeholder="Message"
          className="send-message__inp"
          type="text"
          onChange={(ev: any) => setValue(ev.target.value)}
        />
        <button
          className="send-message__btn"
          onClick={(ev) => {
            ev.preventDefault();
            send(value);
          }}
        >
          Send
        </button>
      </form>
    </div>
  );
};

export default SendMessageForm;
