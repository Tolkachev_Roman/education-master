import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import DialogMessage from './DialogMessage';
import SendMessageForm from './SendMessageForm';

const DialogContentArea = (props: any) => {
  if (props.dialogContent === undefined) {
    return null;
  } else
    return (
      <div className="dialog-content-container">
        {props.dialogContent.content.messages.map((item: any, i: any) => {
          return <DialogMessage key={i} text={item.content} />;
        })}
        <SendMessageForm clientKey={props.key} />
      </div>
    );
};

const mapStateToProps = (state: any) => ({
  dialogContent: state.dialogContent,
});

const DialogContentArea_w = connect(mapStateToProps, null)(DialogContentArea);

export default DialogContentArea_w;
