import React, { useEffect, useState } from 'react';

const Dialog = (props: any) => {
  useEffect(() => {
    if (props.status === 'not active' && document.querySelector('.dialog-preview__checkbox') != null) {
      document.querySelector('.dialog-preview__checkbox').classList.add('checked');
      document.querySelector('.dialog-preview__checkbox').setAttribute('checked', 'checked');
    }
  });

  return (
    <div
      className="dialog-preview"
      onClick={(ev) => {
        const target = ev.target as HTMLElement;
        if (
          target.getAttribute('id') != 'dialog-preview__checkbox_save' ||
          target.getAttribute('id') != 'dialog-preview__checkbox_finished'
        )
          props.showContent();
        else props.setStatus(target);
      }}
    >
      <img src="" alt="" className="avatar" />
      <div className="dialog-preview__text-content">
        <h2 className="dialog-preview__title">{props.name}</h2>
        <p className="dialog-preview__text">{props.text}</p>
      </div>
      {props.isSaved === true ? (
        <input
          id="dialog-preview__checkbox_save"
          type="checkbox"
          className="checked"
          defaultChecked
          onClick={(ev) => props.setStatus(ev.target)}
        />
      ) : (
        <input id="dialog-preview__checkbox_save" type="checkbox" onClick={(ev) => props.setStatus(ev.target)} />
      )}
      <label htmlFor="dialog-preview__checkbox_save">Save</label>
      {props.status === 'finished' ? (
        <input
          id="dialog-preview__checkbox_finished"
          type="checkbox"
          className="checked"
          defaultChecked
          onClick={(ev) => props.setStatus(ev.target)}
        />
      ) : (
        <input id="dialog-preview__checkbox_finished" type="checkbox" onClick={(ev) => props.setStatus(ev.target)} />
      )}
      <label htmlFor="dialog-preview__checkbox_finished">Finished</label>
    </div>
  );
};

export default Dialog;
