import React, { useEffect, useMemo, useState } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { getAllChats } from '../app/actionCreators/getAllChats';
import Dialog from './Dialog';
import firebase from 'firebase';
import { connect } from 'react-redux';
import { setDialogContent } from '../app/actionCreators/setDialogContent';

const SavedList = (props: any) => {
  const [isLoad, setLoad] = useState(true);

  useEffect(() => {
    props.getAllChats();
    setLoad(true);

    props.getAllChats();
  }, [isLoad]);

  const showContent = (key: any) => {
    props.fullChats.map((item: any, i: any) => {
      if (key === i) {
        item = {
          key: key,
          content: item,
        };
        props.setDialogContent(item);
      }
    });
  };

  const mapDialogs = () => {
    let items: any;
    props.fullChats != undefined &&
      props.fullChats.map((dialog: any, key: any) => {
        if (dialog.isSaved === true)
          items.push(
            <Dialog
              key={key}
              name={dialog.clientName}
              text={dialog.messages[dialog.messages.length - 1].content}
              showContent={showContent.bind(this, key)}
              setStatus={setStatus.bind(this, key)}
              status={dialog.status}
            />,
          );
      });
    setLoad(false);
    return items;
  };

  const setStatus = (key: any, checkbox: any) => {
    if (checkbox.getAttribute('id') === 'dialog-preview__checkbox_save') {
      checkbox.classList.toggle('checked');
      if (checkbox.classList.contains('checked'))
        try {
          firebase
            .database()
            .ref('/dialogs/' + key)
            .update({ isSaved: true });
        } catch (error) {
          console.log(error);
        }
      else {
        try {
          firebase
            .database()
            .ref('/dialogs/' + key)
            .update({ isSaved: false });
        } catch (error) {
          console.log(error);
        }
      }
    }

    if (checkbox.getAttribute('id') === 'dialog-preview__checkbox_finished') {
      checkbox.classList.toggle('checked');
      if (checkbox.classList.contains('checked'))
        try {
          firebase
            .database()
            .ref('/dialogs/' + key)
            .update({ status: 'finished' });
        } catch (error) {
          console.log(error);
        }
      else {
        try {
          firebase
            .database()
            .ref('/dialogs/' + key)
            .update({ status: 'default' });
        } catch (error) {
          console.log(error);
        }
      }
    }
    firebase
      .database()
      .ref('/dialogs/' + key)
      .get()
      .then((snapshot) => {
        console.log('NEW DATA', snapshot.val());
      });
    setLoad(false);
  };

  return (
    <div className="dialogs-container">
      <h2>Saved dialogs</h2>
      {props.fullChats != undefined &&
        props.fullChats.map((dialog: any, key: any) => {
          if (dialog.isSaved === true)
            return (
              <Dialog
                key={key}
                name={dialog.clientName}
                text={dialog.messages[dialog.messages.length - 1].content}
                showContent={showContent.bind(this, key)}
                setStatus={setStatus.bind(this, key)}
                status={dialog.status}
                isSaved={dialog.isSaved}
              />
            );
        })}
    </div>
  );
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators(
    {
      getAllChats: getAllChats,
      setDialogContent: setDialogContent,
    },
    dispatch,
  );
};

const mapStateToProps = (state: any) => {
  return {
    fullChats: state.fullChats,
  };
};

const SavedList_w = connect(mapStateToProps, mapDispatchToProps)(SavedList);

export default SavedList_w;
